# NHS eReferral Service Data Extraction

This Python script is designed to connect to the NHS eReferral Service, retrieve the referral worklist "REFERRALS_FOR_REVIEW," and then fetch the referral details and any attachments for each referral. The retrieved data is then converted into a pandas DataFrame and saved to a CSV file named "output.csv."


## Requirements

To run this script, you need the following dependencies:

Python 3.x
The pandas library
The requests library
The tika library
The pathlib library
You can install the required libraries using the following command:
```
pip install pandas requests tika pathlib
```

# Usage

Replace the USER_ID variable with your user ID. This ID is used for authentication with the NHS eReferral Service.

Replace the Authorization header in the _get_headers method with your bearer token. This token is also necessary for authentication.

Run the script. It will perform the following steps:

a. Fetch the referral worklist from the NHS eReferral Service.

b. For each referral in the worklist:

- Fetch the referral details.
- Fetch any attachments associated with the referral.
- Save each attachment as a separate file and convert its content to text using the tika library.
- Create a row in the DataFrame containing the flattened referral details and the parsed text of the attachments.
- Delete temp files

c. Save the DataFrame to a CSV file named "output.csv."

## Explanation of Classes and Methods

### flatten_json(y)

This function is used to flatten nested JSON objects. It takes a dictionary y as input and recursively flattens the keys by concatenating them with underscores. The output is a flat dictionary.

### NHSServiceClient

This class is responsible for interacting with the NHS eReferral Service API. It provides methods to get the referral worklist, get the referral details, and get attachments associated with a referral.

 - API_URL: The base URL of the NHS eReferral Service API.

 - _get_headers(self, role: str, user_id: str) -> Dict[str, str]: Private method to get the HTTP headers required for API requests. It includes the bearer token, user ID, and other necessary headers.

 - get_worklist(self) -> List[Dict]: Retrieves the referral worklist "REFERRALS_FOR_REVIEW" from the API and returns it as a list of dictionaries.

 - get_referral(self, referral_id: str) -> Dict: Fetches the details of a specific referral using its referral_id and returns the response as a dictionary.

 - get_attachments(self, referral: Dict) -> List[str]: Fetches attachments associated with a referral and returns them as a list of binary content.

 - process_referrals(client: NHSServiceClient, worklist: List[Dict]) -> pd.DataFrame

    This function processes the referral worklist fetched from the NHS eReferral Service. It takes an instance of NHSServiceClient and the worklist as input. It iterates over each referral in the worklist, fetches its details and attachments, and converts the attachment content to text. The results are stored in a pandas DataFrame.

 - main()

    The main function of the script. It creates an instance of NHSServiceClient, fetches the referral worklist, processes the referrals, and saves the resulting DataFrame to a CSV file named "output.csv."

### Script Execution

    The script's main block runs the main() function, which executes the entire data extraction process. After running the script, you should find a file named "output.csv" containing the extracted data in CSV format.