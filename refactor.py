import logging
import requests
from requests import RequestException
import uuid
import base64
from fhirclient import client
from time import time
import jwt 

nhs_access_token = None

class GPConnectError(Exception):
  pass

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class NHSServiceClient:

    def __init__(self):
        self.api_url = "https://sandbox.api.service.nhs.uk"
        self.user_id = "021600556514"
    
    def get_headers(self, role, user_id):
        """Returns authenticated headers for calling NHS APIs"""

        nhs_bearer_token = get_nhs_access_token()
        headers = {
            "Authorization": "Bearer {nhs_bearer_token}",  
            "X-ERS-UserId": user_id,
            "X-ERS-Roles": role,
            "accept": "application/fhir+json",
            "NHSD-End-User-Organisation-ODS": "R69",
            "content-type": "application/fhir+json",
            "X-Request-ID": str(uuid.uuid4())
        }
        return headers   

    def fetch_worklist(self):
        """Fetches the referral worklist"""
        headers = self.get_headers("SERVICE_PROVIDER_CLINICIAN_ADMIN", self.user_id)
        
        params = {
            "resourceType": "Parameters",
            "meta": {
                "profile": [
                "https://fhir.nhs.uk/STU3/StructureDefinition/eRS-FetchWorklist-Parameters-1"
                ]
            },
            "parameter": [
                {
                "name": "listType",
                "valueCodeableConcept": {
                    "coding": [
                    {
                        "system": "https://fhir.nhs.uk/STU3/CodeSystem/eRS-ReferralListSelector-1",
                        "code": "REFERRALS_FOR_REVIEW"
                    }
                    ]
                }
                }
            ]
        }
        
        logger.info(f"Connecting to eRS to download worklist")

        response = requests.post(f"{self.api_url}/referrals/FHIR/STU3/ReferralRequest/$ers.fetchworklist", 
                                 headers=headers, json=params)
                                 
        if response.status_code != 200:
           logger.error(f"Error fetching worklist: {response.text}")
           return None
           
        logger.info("Fetch worklist successful")
        worklist = response.json()  
        return worklist['entry']

    def fetch_referral(self, referral_id):
        """Fetches a single referral by ID"""
        # Fetch referral details
        headers = self.get_headers("SERVICE_PROVIDER_CLINICIAN_ADMIN", self.user_id)
        
        logger.info(f"Connecting to eRS to download referral {referral_id}")

        response = requests.get(f"{self.api_url}/referrals/FHIR/STU3/ReferralRequest/{referral_id}", 
                                headers=headers)
                                
        if response.status_code != 200:
            logger.error(f"Error fetching referral {referral_id}: {response.text}")
            return None

        logger.info(f"Fetched referral {referral_id} successfully")
        return response.json()

    def fetch_attachments(self, referral):
        """Fetches all attachments for a referral"""
        attachments = []
        
        headers = self.get_headers("SERVICE_PROVIDER_CLINICIAN_ADMIN", self.user_id)
        
        logger.info(f"Connecting to eRS to download attachments")

        for resource in referral.get("contained", []):
            if resource["resourceType"] == "DocumentReference":
                url = resource["content"][0]["attachment"]["url"]
                response = requests.get(f"{self.api_url}//referrals/FHIR/STU3/{url}", headers=headers)
                if response.status_code != 200:
                    logger.error(f"Error fetching attachment {url}: {response.text}")
                else:
                    attachments.append(response.content)
                    logger.info(f"Fetched attachment {url} successfully")
        return attachments

    def fetch_patient(self, patient_id):
        """Fetches patient demographics by NHS Number"""
        headers = self.get_headers("SERVICE_PROVIDER_CLINICIAN_ADMIN", self.user_id)
        
        logger.info(f"Connecting to Personal Demographics Service for patient {patient_id}")

        patient_id = "9000000009" #override NHS number for NHS Sandbox

        response = requests.get(f"{self.api_url}/personal-demographics/FHIR/R4/Patient/{patient_id}", 
                                headers=headers)
        
        if response.status_code != 200:
           logger.error(f"Error fetching patient {patient_id}: {response.text}")
           return None
        logger.info(f"Fetched patient demographics successfully")   
        return response.json()
        
def fetch_gp_connect_record(patient_id):
  
  logger.info(f"Connecting to GP Record for {patient_id}")
  patient_id = "9690937286"
  nhs_bearer_token = "get_nhs_access_token()"
  headers = {
    "accept": "application/fhir+json",
    "Content-Type": "application/fhir+json",  
    "Ssp-TraceID": str(uuid.uuid4()),
    "Ssp-From": "200000000359", 
    "Ssp-To": "918999198738",
    "Ssp-InteractionID": "urn:nhs:names:services:gpconnect:fhir:operation:gpc.getstructuredrecord-1",
    "Authorization": "Bearer {nhs_bearer_token}"
  }

  data = {
    "resourceType": "Parameters",
    "parameter": [
        {
        "name": "patientNHSNumber",
        "valueIdentifier": {
            "system": "https://fhir.nhs.uk/Id/nhs-number",
            "value": patient_id
        }
      },
      {
        "name": "includeAllergies",
        "part": [
          {
            "name": "includeResolvedAllergies",
            "valueBoolean": True
          }
        ]
      },
      {
        "name": "includeMedication"
      },
      {
        "name": "includeConsultations",
        "part": [
          {
            "name": "includeNumberOfMostRecent",
            "valueInteger": 3  
          }
        ]
      },
      {
        "name": "includeProblems"
      },
      {
        "name": "includeImmunisations"
      },
      {
        "name": "includeUncategorisedData" 
      },
      {
        "name": "includeInvestigations"
      },
      {
        "name": "includeReferrals"
      }
    ]
  }

  
  try:  
    response = requests.post(
    "https://orange.testlab.nhs.uk/B82617/STU3/1/gpconnect/structured/fhir/Patient/$gpc.getstructuredrecord",
        headers=headers,
        json=data
    )

  except RequestException as e:
        logger.error("Request error: %s", e)

  else:

    if response.ok:
        logger.info("GP record retrieved")
           
    else:
        logger.error("Error status: %s", response.status_code)
        logger.error("Error code: %s", response.json()["issue"][0]["details"]["coding"][0]["code"])

        raise GPConnectError(response.text)

    return response.json()
  
    
def create_bundle(referral, attachments, patient, gp_record):
    """Creates a FHIR bundle with all data to submit"""
    
    bundle = {...} # Create bundle
    
    return bundle

def get_nhs_access_token():

    global nhs_access_token
    
    if nhs_access_token:
        logger.debug(f"NHS Access token is {nhs_access_token}")
        return nhs_access_token

    # Get new token
    nhs_access_token = retrieve_new_token()
    
    return nhs_access_token

def retrieve_new_token():

    # Securely load private key 
    with open("certs/test-1.pem", "r") as f:
        private_key = f.read()
    
    # Generate JWT
    jwt_token = generate_jwt(private_key) 
    logger.debug(f"JWT token is {jwt_token}")
    headers = {
        "content-type": "application/x-www-form-urlencoded",
        "grant_type": "client_credentials",
        "client_assertion_type": "urn:ietf:params:oauth:client-assertion-type:jwt-bearer",
        "client_assertion": jwt_token,
        }
    
    # Request access token
    try:
        response = requests.post("https://dev.api.service.nhs.uk/oauth2/token", headers)

        if response.status_code == 400:
        # Bad request, raise with error message
            raise Exception(response.json()["error_description"])
    
        elif response.status_code == 401:
            # Unauthorized, raise with error message
            raise Exception(response.json()["error_description"])

        elif not response.ok:
            # Other error
            raise Exception("Error retrieving access token")

    except requests.RequestException as e:
        # Network error
        raise Exception(str(e))
    else:       
       # Get JSON body
        json_data = response.json()

        # Extract access_token 
        access_token = json_data['access_token']

        return access_token

def generate_jwt(private_key):

    claims = {
    "sub": "xGWpmGA4yQDJ90IrC0R2R9cdpcFAI6vK",
    "iss": "xGWpmGA4yQDJ90IrC0R2R9cdpcFAI6vK",
    "jti": str(uuid.uuid4()),
    "aud": "https://dev.api.service.nhs.uk/oauth2/token",
    "exp": int(time()) + 300, # 5mins in the future
    "reason_for_request": "directcare"
    }

    additional_headers = {"kid": "test-1"}

    token = jwt.encode(
    claims, private_key, algorithm="RS512", headers=additional_headers
    )

    return token


# def submit_bundle(bundle):
#     """Submits a FHIR bundle to Azure API"""
    
#     # Call Azure API
    
#     if response.status_code != 200:
#         logger.error("Error submitting bundle")
#     else:
#         logger.info("Bundle submitted successfully")
        



def main():

    client = NHSServiceClient()
    worklist = client.fetch_worklist()
    
    if worklist is None:
        logger.error("No worklist entries found")
        return

    logger.info(f"{len(worklist)} found to be pending review")

    for referral in worklist:
        if 'item' in referral and 'reference' in referral['item']:
            referral_id_with_prefix = referral['item']['reference']
            # Split the reference by '/' and get the second part as the referral_id
            referral_id = referral_id_with_prefix.split('/')[1]
        
        referral = client.fetch_referral(referral_id)
        if referral is None:
            logger.error(f"Failed to fetch referral {referral_id}")
            continue
            
        attachments = client.fetch_attachments(referral)
        
        patient_id = referral["subject"]["identifier"]["value"]
        logger.info(f"Patient detected {patient_id}")
        patient = client.fetch_patient(patient_id)
        if patient is None:
            logger.error(f"Failed to fetch patient {patient_id}")
            continue
            
        gp_record = fetch_gp_connect_record(patient_id)
        if gp_record is None:
            logger.error(f"Failed to fetch GP record for {patient_id}")
            continue
        
        logger.info(f"Sending data to Digital Twin for processing")
        # bundle = create_bundle(referral, attachments, patient, gp_record)
        # submit_bundle(bundle)
        
if __name__ == "__main__":
    main()