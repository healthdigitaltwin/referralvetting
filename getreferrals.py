# This code connects to NHS eReferral Service, pulls the referral worklist "REFERRALS_FOR_REVIEW", then for each referral pulls the referral details and any attachments, which are then converted to text.  The output is a dataframe.


import logging
import requests
import pandas as pd
import uuid
from typing import Dict, List
from pathlib import Path
from tika import parser
import os

# Set up logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class NHSServiceClient:
    API_URL = "https://sandbox.api.service.nhs.uk"
    USER_ID = "021600556514"  # Replace with your user ID

    def _get_headers(self, role: str, user_id: str) -> Dict[str, str]:
        return {
            "Authorization": "Bearer g1112R_ccQ1Ebbb4gtHBP1aaaNM",  # Replace with your token
            "X-ERS-UserId": user_id,
            "X-ERS-Roles": role
        }

    def get_worklist(self) -> List[Dict]:
        headers = self._get_headers("SERVICE_PROVIDER_CLINICIAN_ADMIN", self.USER_ID)
        headers["nhsd-end-user-organisation-ods"] = "R69"
        headers["nhsd-ers-business-function"] = "SERVICE_PROVIDER_CLINICIAN_ADMIN"
        headers["nhsd-ers-on-behalf-of-user-id"] = self.USER_ID
        headers["x-correlation-id"] = str(uuid.uuid4())
        headers["content-type"] = "application/fhir+json"

        body = {
            "resourceType": "Parameters",
            "meta": {
                "profile": ["https://fhir.nhs.uk/STU3/StructureDefinition/eRS-FetchWorklist-Parameters-1"]
            },
            "parameter": [{
                "name": "listType",
                "valueCodeableConcept": {
                    "coding": [{
                        "system": "https://fhir.nhs.uk/STU3/CodeSystem/eRS-ReferralListSelector-1",
                        "code": "REFERRALS_FOR_REVIEW"
                    }]
                }
            }]
        }

        response = requests.post(
            f"{self.API_URL}/referrals/FHIR/STU3/ReferralRequest/$ers.fetchworklist",
            headers=headers,
            json=body
        )
        if response.status_code != 200:
            logger.error(f"Error in get_worklist: {response.status_code}, response: {response.text}")
            return []
        logger.info(f"Response code from get_worklist: {response.status_code}")
        worklist = response.json()
        return worklist['entry']

    def get_referral(self, referral_id: str) -> Dict:
        headers = self._get_headers("SERVICE_PROVIDER_CLINICIAN_ADMIN", self.USER_ID)
        response = requests.get(f"{self.API_URL}/referrals/FHIR/STU3/ReferralRequest/{referral_id}", headers=headers)
        if response.status_code != 200:
            logger.error(f"Error in get_referral for referral_id {referral_id}: {response.status_code}")
            return {}
        logger.info(f"Response code from get_referral for referral_id {referral_id}: {response.status_code}")
        return response.json()

    def get_attachments(self, referral: Dict) -> List[str]:
        headers = self._get_headers("SERVICE_PROVIDER_CLINICIAN_ADMIN", self.USER_ID)
        attachments = []
        for resource in referral.get('contained', []):
            if resource['resourceType'] == 'DocumentReference':
                binary_url = resource['content'][0]['attachment']['url']
                response = requests.get(f"{self.API_URL}/referrals/FHIR/STU3/{binary_url}", headers=headers)
                if response.status_code != 200:
                    logger.error(f"Error in get_attachments for binary_url {binary_url}: {response.status_code}")
                    continue
                logger.info(f"Successfully fetched attachment from {binary_url}")
                attachments.append(response.content)
        return attachments

def main():
    client = NHSServiceClient()
    worklist = client.get_worklist()

if __name__ == "__main__":
    main()
