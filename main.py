import logging
import requests
import pandas as pd
import uuid
from typing import Dict, List, Optional
from pathlib import Path
from tika import parser
import os
from getpass import getpass
import json
import base64
from fhir.resources.bundle import Bundle, BundleEntry, BundleLink, BundleEntryRequest
from fhir.resources.documentreference import DocumentReference
from fhir.resources.parameters import Parameters
from fhir.resources.coding import Coding
from fhir.resources.codeableconcept import CodeableConcept

# Set up logging
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

class NHSServiceClient:
    """A class to interact with NHS eReferral Service."""

    API_URL = "https://sandbox.api.service.nhs.uk"
    USER_ID = "021600556514"  # Replace with your user ID

    def _get_headers(self, role: str, user_id: str) -> Dict[str, str]:
        """
        Helper function to get the request headers.
        
        :param role: the role of the user
        :param user_id: the id of the user
        :return: A dictionary of headers
        """
        return {
            "Authorization": "Bearer g1112R_ccQ1Ebbb4gtHBP1aaaNM",  # Replace with your token
            "X-ERS-UserId": user_id,
            "X-ERS-Roles": role,
            "X-Request-ID": "",
            "NHSD-End-User-Organisation-ODS": "R69" 
        }
    
    def get_worklist(self) -> Optional[List[Dict]]:
        """
        Fetch the referral worklist.

        :return: A list of referrals in the worklist. Returns None if an error occurred.
        """
        headers = self._get_headers("SERVICE_PROVIDER_CLINICIAN_ADMIN", self.USER_ID)
        headers["nhsd-end-user-organisation-ods"] = "R69"
        headers["nhsd-ers-business-function"] = "SERVICE_PROVIDER_CLINICIAN_ADMIN"
        headers["nhsd-ers-on-behalf-of-user-id"] = self.USER_ID
        headers["x-correlation-id"] = str(uuid.uuid4())
        headers["content-type"] = "application/fhir+json"

        body = {
            "resourceType": "Parameters",
            "meta": {
                "profile": ["https://fhir.nhs.uk/STU3/StructureDefinition/eRS-FetchWorklist-Parameters-1"]
            },
            "parameter": [{
                "name": "listType",
                "valueCodeableConcept": {
                    "coding": [{
                        "system": "https://fhir.nhs.uk/STU3/CodeSystem/eRS-ReferralListSelector-1",
                        "code": "REFERRALS_FOR_REVIEW"
                    }]
                }
            }]
        }

        response = requests.post(
            f"{self.API_URL}/referrals/FHIR/STU3/ReferralRequest/$ers.fetchworklist",
            headers=headers,
            json=body
        )
        if response.status_code != 200:
            logger.error(f"Error in get_worklist: {response.status_code}, response: {response.text}")
            return None
        logger.info(f"Successfully downloaded eRS Worklist: {response.status_code}")
        worklist = response.json()
        return worklist['entry']


    def get_referral(self, referral_id: str) -> Optional[Dict]:
        """
        Fetch the details of a referral.

        :param referral_id: the id of the referral
        :return: A dictionary of referral details. Returns None if an error occurred.
        """
        headers = self._get_headers("SERVICE_PROVIDER_CLINICIAN_ADMIN", self.USER_ID)
        response = requests.get(f"{self.API_URL}/referrals/FHIR/STU3/ReferralRequest/{referral_id}", headers=headers)
        if response.status_code != 200:
            logger.error(f"Error in get_referral for referral_id {referral_id}: {response.status_code}")
            return None
        logger.info(f"Successfully downloaded referral {referral_id}: {response.status_code}")
        return response.json()

    def get_attachments(self, referral: Dict) -> List[str]:
        """
        Fetch the attachments of a referral.

        :param referral: the referral details
        :return: A list of attachments
        """
        headers = self._get_headers("SERVICE_PROVIDER_CLINICIAN_ADMIN", self.USER_ID)
        attachments = []
        for resource in referral.get('contained', []):
            if resource['resourceType'] == 'DocumentReference':
                binary_url = resource['content'][0]['attachment']['url']
                response = requests.get(f"{self.API_URL}/referrals/FHIR/STU3/{binary_url}", headers=headers)
                if response.status_code != 200:
                    logger.error(f"Error in get_attachments for binary_url {binary_url}: {response.status_code}")
                    continue
                logger.info(f"Successfully fetched attachment from {binary_url}: {response.status_code}")
                attachments.append(response.content)
        return attachments
    
    def get_patient(self, patient_id: str) -> Optional[Dict]:
        """
        Fetch the details of a patient.

        :param patient_id: the id of the patient
        :return: A dictionary of patient details. Returns None if an error occurred.
        """
        headers = self._get_headers("SERVICE_PROVIDER_CLINICIAN_ADMIN", self.USER_ID)
        logger.debug(headers)
        response = requests.get(f"{self.API_URL}/personal-demographics/FHIR/R4/Patient/{patient_id}", headers=headers)
        if response.status_code != 200:
            logger.error(f"Error in get_patient for patient_id {patient_id}: {response.status_code}")
            return None
        logger.info(f"Successfully fetched Patient Demographics for {patient_id}: {response.status_code}")
        return response.json()
    
def get_gp_connect_record(patient_nhs_number: str) -> Optional[Dict]:
    """
    Fetch the patient's medical record from NHS GP Connect.

    :param patient_nhs_number: the NHS number of the patient
    :return: A dictionary representing the patient's medical record. Returns None if an error occurred.
    """
    headers = {
        "accept": "application/fhir+json",
        "Content-Type": "application/fhir+json",
        "Ssp-TraceID": str(uuid.uuid4()),
        "Ssp-From": "200000000359",
        "Ssp-To": "918999198738",
        "Ssp-InteractionID": "urn:nhs:names:services:gpconnect:fhir:operation:gpc.getstructuredrecord-1",
        "Authorization": "Bearer eyJhbGciOiJub25lIiwidHlwIjoiSldUIn0.ewogICJpc3MiOiAiaHR0cHM6Ly9vcmFuZ2UudGVzdGxhYi5uaHMudWsvIiwKICAic3ViIjogIjEiLAogICJhdWQiOiAiaHR0cHM6Ly9vcmFuZ2UudGVzdGxhYi5uaHMudWsvQjgyNjE3L1NUVTMvMS9ncGNvbm5lY3QvZG9jdW1lbnRzL2ZoaXIiLAogICJleHAiOiAxNjkwMjgwNTQyLAogICJpYXQiOiAxNjkwMjgwMjQyLAogICJyZWFzb25fZm9yX3JlcXVlc3QiOiAiZGlyZWN0Y2FyZSIsCiAgInJlcXVlc3RlZF9zY29wZSI6ICJwYXRpZW50LyoucmVhZCIsCiAgInJlcXVlc3RpbmdfZGV2aWNlIjogewogICAgInJlc291cmNlVHlwZSI6ICJEZXZpY2UiLAogICAgImlkZW50aWZpZXIiOiBbCiAgICAgIHsKICAgICAgICAic3lzdGVtIjogImh0dHBzOi8vb3JhbmdlLnRlc3RsYWIubmhzLnVrL2dwY29ubmVjdC1kZW1vbnN0cmF0b3IvSWQvbG9jYWwtc3lzdGVtLWluc3RhbmNlLWlkIiwKICAgICAgICAidmFsdWUiOiAiZ3BjZGVtb25zdHJhdG9yLTEtb3JhbmdlIgogICAgICB9CiAgICBdLAogICAgIm1vZGVsIjogIkdQIENvbm5lY3QgRGVtb25zdHJhdG9yIiwKICAgICJ2ZXJzaW9uIjogIjEuNS4wIgogIH0sCiAgInJlcXVlc3Rpbmdfb3JnYW5pemF0aW9uIjogewogICAgInJlc291cmNlVHlwZSI6ICJPcmdhbml6YXRpb24iLAogICAgImlkZW50aWZpZXIiOiBbCiAgICAgIHsKICAgICAgICAic3lzdGVtIjogImh0dHBzOi8vZmhpci5uaHMudWsvSWQvb2RzLW9yZ2FuaXphdGlvbi1jb2RlIiwKICAgICAgICAidmFsdWUiOiAiQTExMTExIgogICAgICB9CiAgICBdLAogICAgIm5hbWUiOiAiQ29uc3VtZXIgb3JnYW5pc2F0aW9uIG5hbWUiCiAgfSwKICAicmVxdWVzdGluZ19wcmFjdGl0aW9uZXIiOiB7CiAgICAicmVzb3VyY2VUeXBlIjogIlByYWN0aXRpb25lciIsCiAgICAiaWQiOiAiMSIsCiAgICAiaWRlbnRpZmllciI6IFsKICAgICAgewogICAgICAgICJzeXN0ZW0iOiAiaHR0cHM6Ly9maGlyLm5ocy51ay9JZC9zZHMtdXNlci1pZCIsCiAgICAgICAgInZhbHVlIjogIjExMTExMTExMTExMSIKICAgICAgfSwKICAgICAgewogICAgICAgICJzeXN0ZW0iOiAiaHR0cHM6Ly9maGlyLm5ocy51ay9JZC9zZHMtcm9sZS1wcm9maWxlLWlkIiwKICAgICAgICAidmFsdWUiOiAiMjIyMjIyMjIyMjIyMjIiCiAgICAgIH0sCiAgICAgIHsKICAgICAgICAic3lzdGVtIjogImh0dHBzOi8vb3JhbmdlLnRlc3RsYWIubmhzLnVrL2dwY29ubmVjdC1kZW1vbnN0cmF0b3IvSWQvbG9jYWwtdXNlci1pZCIsCiAgICAgICAgInZhbHVlIjogIjEiCiAgICAgIH0KICAgIF0sCiAgICAibmFtZSI6IFsKICAgICAgewogICAgICAgICJmYW1pbHkiOiAiRGVtb25zdHJhdG9yIiwKICAgICAgICAiZ2l2ZW4iOiBbCiAgICAgICAgICAiR1BDb25uZWN0IgogICAgICAgIF0sCiAgICAgICAgInByZWZpeCI6IFsKICAgICAgICAgICJEciIKICAgICAgICBdCiAgICAgIH0KICAgIF0KICB9Cn0."
    }
    logger.debug(headers)
    data = {
        "resourceType": "Parameters",
        "parameter": [
            {
                "name": "patientNHSNumber",
                "valueIdentifier": {
                    "system": "https://fhir.nhs.uk/Id/nhs-number",
                    "value": patient_nhs_number,
                },
            },
            {
                "name": "includeAllergies",
                "part": [
                    {
                        "name": "includeResolvedAllergies",
                        "valueBoolean": True,
                    }
                ],
            },
            {
                "name": "includeMedication",
            },
            {
                "name": "includeConsultations",
                "part": [
                    {
                        "name": "includeNumberOfMostRecent",
                        "valueInteger": 3,
                    }
                ],
            },
            {
                "name": "includeProblems",
            },
            {
                "name": "includeImmunisations",
            },
            {
                "name": "includeUncategorisedData",
            },
            {
                "name": "includeInvestigations",
            },
            {
                "name": "includeReferrals",
            },
        ],
    }
    logger.debug(data)
    response = requests.post(
        "https://orange.testlab.nhs.uk/B82617/STU3/1/gpconnect/structured/fhir/Patient/$gpc.getstructuredrecord",
        headers=headers,
        json=data,
    )

    if response.status_code != 200:
        logger.error(f"Error in get_gp_connect_record: {response.status_code}, response: {response.text}")
        return None
    logger.info(f"Response code from get_gp_connect_record: {response.status_code}")
    return response.json()


def get_attachment_entries(referral_details, attachments):
    attachment_entries = []
    for attachment in attachments:
        stripped_content = base64.b64encode(attachment).decode('utf-8')
    return attachment_entries

def post_to_azure(api_url, access_token, bundle_json):
    headers = {
        "Authorization": f"Bearer {access_token}",
        "Content-Type": "application/fhir+json"
    }
    response = requests.post(api_url, headers=headers, data=bundle_json)

    if response.status_code == 200:
        logger.info("Bundle successfully submitted to Azure.")
    else:
        logger.error(f"Failed to submit the bundle to Azure. Response: {response.text}")

def main():
    client = NHSServiceClient()

    # Fetch the referral worklist
    referral_worklist = client.get_worklist()
    if referral_worklist is None:
        logger.error("Failed to fetch the worklist")
        return
    # print(referral_worklist)

       # Fetch the details of each referral in the worklist
    for referral in referral_worklist:
        if 'item' in referral and 'reference' in referral['item']:
            referral_id_with_prefix = referral['item']['reference']
            # Split the reference by '/' and get the second part as the referral_id
            referral_id = referral_id_with_prefix.split('/')[1]
            referral_details = client.get_referral(referral_id)
            # print(referral_details)
            if referral_details is None:
                logger.error(f"Failed to fetch details for referral_id {referral_id}")
                continue
            # Put any code that depends on referral_details here
            # Extract patient_id from referral_details
            # print(referral_details)
            patient_id = referral_details.get('subject', {}).get('identifier', {}).get('value', '')
            logger.info(f"Patient ID detected {patient_id}")
            if patient_id:
                patient_id = "9000000009" #hard coded NHS number to develop against NHS Sandbox
                logger.info(f"Fetching demographics for {patient_id}")
                patient = client.get_patient(patient_id) 
                if patient:
                    logger.info(f"Demographics for {patient_id} received")
                    patient_id = "9690937286" #hard coded NHS number to develop against NHS Sandbox
                    gp_connect_record = get_gp_connect_record(patient_id)
                    if gp_connect_record:
                        logger.info(f"Patient's medical record received from GP Connect.")
                        # Process the GP Connect record as needed.
                        # You can access different parts of the record using gp_connect_record dictionary.
                    else:
                        logger.error(f"Failed to fetch patient's medical record from GP Connect.")
            else:   
                logger.error(f"Failed to fetch patient id")

            attachments = client.get_attachments(referral_details)
        else:
            logger.warning("Failed to retrieve referral ID: 'item' or 'reference' not found.")
            continue

       

if __name__ == "__main__":
    main()
