import logging
import requests
from requests import RequestException
import uuid
from fhirclient import client
from time import time
import jwt 

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class GPConnectError(Exception):
    pass

class BaseServiceClient:

    def __init__(self, api_url, user_id):
        self.api_url = api_url
        self.user_id = user_id
        self.token = self._retrieve_new_token()

    def _get_headers(self, role):
        headers = {
            "Authorization": f"Bearer {self.token}",  
            "X-ERS-UserId": self.user_id,
            "X-ERS-Roles": role,
            "accept": "application/fhir+json",
            "NHSD-End-User-Organisation-ODS": "R69",
            "content-type": "application/fhir+json",
            "X-Request-ID": str(uuid.uuid4())
        }
        return headers 

    def _retrieve_new_token(self):
        # This function retrieves a new token
        # Implementation of this function is left as an exercise for the reader
        pass

class NHSServiceClient(BaseServiceClient):

    def fetch_worklist(self):
        """Fetches the referral worklist"""
        headers = self._get_headers("SERVICE_PROVIDER_CLINICIAN_ADMIN")
        params = self._get_worklist_params()
        response = self._post_request(f"{self.api_url}/referrals/FHIR/STU3/ReferralRequest/$ers.fetchworklist", headers, params)
        worklist = response.json()  
        return worklist['entry']

    def fetch_referral(self, referral_id):
        """Fetches a single referral by ID"""
        logger.info(f"Feteching referral {referral_id}")
        headers = self._get_headers("SERVICE_PROVIDER_CLINICIAN_ADMIN")
        response = self._get_request(f"{self.api_url}/referrals/FHIR/STU3/ReferralRequest/{referral_id}", headers)
        return response.json()

    def fetch_attachments(self, referral):
        """Fetches all attachments for a referral"""
        attachments = []
        headers = self._get_headers("SERVICE_PROVIDER_CLINICIAN_ADMIN")
        for resource in referral.get("contained", []):
            if resource["resourceType"] == "DocumentReference":
                url = resource["content"][0]["attachment"]["url"]
                response = self._get_request(f"{self.api_url}//referrals/FHIR/STU3/{url}", headers)
                attachments.append(response.content)
        return attachments

    def fetch_patient(self, patient_id):
        """Fetches patient demographics by NHS Number"""
        patient_id = "9000000009" #override id for NHS Sandbox as it doesn't have matching patients.
        logger.info(f"Feteching patient details for {patient_id}")
        headers = self._get_headers("SERVICE_PROVIDER_CLINICIAN_ADMIN")
        response = self._get_request(f"{self.api_url}/personal-demographics/FHIR/R4/Patient/{patient_id}", headers)
        return response.json()

    def _get_worklist_params(self):

        params = {
            "resourceType": "Parameters",
            "meta": {
                "profile": [
                "https://fhir.nhs.uk/STU3/StructureDefinition/eRS-FetchWorklist-Parameters-1"
                ]
            },
            "parameter": [
                {
                "name": "listType",
                "valueCodeableConcept": {
                    "coding": [
                    {
                        "system": "https://fhir.nhs.uk/STU3/CodeSystem/eRS-ReferralListSelector-1",
                        "code": "REFERRALS_FOR_REVIEW"
                    }
                    ]
                }
                }
            ]
        }
        
        return params

    def _get_request(self, url, headers):
        response = requests.get(url, headers=headers)
        self._check_response(response)
        return response

    def _post_request(self, url, headers, params):
        response = requests.post(url, headers=headers, json=params)
        self._check_response(response)
        return response

    def _check_response(self, response):
        if response.status_code != 200:
            logger.error(f"Error fetching data: {response.text}")
            raise GPConnectError(response.text)

class GPConnectServiceClient(BaseServiceClient):

    def fetch_gp_connect_record(self, patient_id):
        """Fetches GP Connect record"""
        headers = self._get_headers("SERVICE_PROVIDER_CLINICIAN_ADMIN")
        data = self._get_gp_connect_record_params(patient_id)
        response = self._post_request("https://orange.testlab.nhs.uk/B82617/STU3/1/gpconnect/structured/fhir/Patient/$gpc.getstructuredrecord", headers, data)
        return response.json()

    def _get_gp_connect_record_params(self, patient_id):
        # This function returns the parameters for the GP Connect record
        params = {
                "resourceType": "Parameters",
                "parameter": [
                    {
                    "name": "patientNHSNumber",
                    "valueIdentifier": {
                        "system": "https://fhir.nhs.uk/Id/nhs-number",
                        "value": patient_id
                    }
                },
                {
                    "name": "includeAllergies",
                    "part": [
                    {
                        "name": "includeResolvedAllergies",
                        "valueBoolean": True
                    }
                    ]
                },
                {
                    "name": "includeMedication"
                },
                {
                    "name": "includeConsultations",
                    "part": [
                    {
                        "name": "includeNumberOfMostRecent",
                        "valueInteger": 3  
                    }
                    ]
                },
                {
                    "name": "includeProblems"
                },
                {
                    "name": "includeImmunisations"
                },
                {
                    "name": "includeUncategorisedData" 
                },
                {
                    "name": "includeInvestigations"
                },
                {
                    "name": "includeReferrals"
                }
                ]
            }
        
        return params

def create_bundle(referral, attachments, patient, gp_record):
    """Creates a FHIR bundle with all data to submit"""
    bundle = {...} # Create bundle
    return bundle

def main():

    nhs_client = NHSServiceClient("https://sandbox.api.service.nhs.uk", "021600556514")
    gp_connect_client = GPConnectServiceClient("https://sandbox.api.service.nhs.uk", "021600556514")
    worklist = nhs_client.fetch_worklist()
    
    if worklist is None:
        logger.error("No worklist entries found")
        return

    logger.info(f"{len(worklist)} found to be pending review")

    for referral in worklist:
        if 'item' in referral and 'reference' in referral['item']:
            referral_id_with_prefix = referral['item']['reference']
            referral_id = referral_id_with_prefix.split('/')[1]
        
        referral = nhs_client.fetch_referral(referral_id)
        if referral is None:
            logger.error(f"Failed to fetch referral {referral_id}")
            continue
            
        attachments = nhs_client.fetch_attachments(referral)
        
        patient_id = referral["subject"]["identifier"]["value"]
        logger.info(f"Patient detected {patient_id}")
        patient = nhs_client.fetch_patient(patient_id)
        if patient is None:
            logger.error(f"Failed to fetch patient {patient_id}")
            continue
            
        gp_record = gp_connect_client.fetch_gp_connect_record(patient_id)
        if gp_record is None:
            logger.error(f"Failed to fetch GP record for {patient_id}")
            continue
        
        logger.info(f"Sending data to Digital Twin for processing")
        # bundle = create_bundle(referral, attachments, patient, gp_record)
        # submit_bundle(bundle)
        
if __name__ == "__main__":
    main()
